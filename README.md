# Einstieg ins Forschungsdatenmanagement mit git und GitLab

Datum: 09.12.2020, 09:00 - 13:00 Uhr

Format: Flipped Classroom in und mit GitLab, ~1,5h Vorbereitungszeit

## Folien

<a href="./slides">Vortragsfolien</a>